# electron-pivotal-tracker

This is a minimal Electron application based on the [Quick Start Guide](https://electronjs.org/docs/tutorial/quick-start) project; it's been slightly altered to load a Pivotal Tracker project and run in full screen mode. This way it's easy to get back to Pivotal Tracker and avoid it getting lost in a sea of browser tabs, enabling more efficient use of Pivotal Tracker.

## To Use

```bash
# Clone this repository
git clone https://gitlab.com/saywebsolutions/electron-pivotal-tracker.git

# Go into the repository
cd electron-pivotal-tracker

# Install dependencies
npm install

# Create a .env file and add your Pivotal Tracker url
echo 'PIVOTAL_URL="https://www.pivotaltracker.com/n/projects/19"' | tee .env

# Run the app
npm start
```

Enjoy easy alt-tabbing to your Pivotal Tracker project!

## License

[MIT](LICENSE.md)
